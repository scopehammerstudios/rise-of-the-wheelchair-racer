﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;

namespace RiseOfTheWheelchairRacer
{
    enum CharacterState //Manages the state of the character
    {
        standing,
        runningLeft,
        runningRight,
        jumping,
        dead
    };

    enum GameState //Manages the state of the game display
    {
        menu,
        objective,
        controls,
        highScore,
        pause,
        game,
        gameOver,
        difficulty,
        startScreen
    };
    enum MainMenuState //manages state of main menu
    {
        start,
        objective,
        highscore,
        exit,
    };
    enum PauseMenuState //manaages state of pause menu
    {
        resume,
        menu,
        exit,
    };
    enum GameDifficulty //Manages the difficulty of the game
    {
        tutorial,
        easy,
        medium,
        hard,
        hardPlus,
    };

    public static class GameVariables //A class for all game variables to be used elsewhere
    {
        public const int tileSize = 64;
        public static Vector2 screenPosition = new Vector2(-200, -10);
    }
}
