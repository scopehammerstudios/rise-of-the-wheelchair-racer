﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;
using System.Threading;

namespace RiseOfTheWheelchairRacer
{
    class Timer //Class for keeping time and score
    {
        //Fields
        public TimeSpan time;
        private SpriteFont font;
        private TimeSpan score;

        //Properties
        public TimeSpan Score 
        { 
            get { return score; } 
            set { time = score; } 
        }

        //Constructor
        public Timer(SpriteFont font)
        {
            this.font = font;
            time = TimeSpan.FromSeconds(.01);
        }

        //Methods

        // Updates time counter and prints in top left of screen
        public void KeepTime(GameTime gameTime, SpriteBatch spriteBatch, Camera camera, ref Player player)
        {
            //Increases time by gametime
            time += gameTime.ElapsedGameTime;
            if (player.GameWon == true) //If the player beats the game, set their score to their time
            {
                score = time;
            }
            else //If they quit then set their score to zero
            {
                score = TimeSpan.Zero;
            }

            string timeString = time.ToString(@"hh\:mm\:ss\.FF").Trim('0', ':', '0'); //Formats time to printable string on the screen

            Vector2 loc = new Vector2(camera.Center.X + 10, camera.Center.Y + 10);
            spriteBatch.DrawString(font, timeString, loc, Color.White); //Prints the string onto the screen
        }

    }
}
