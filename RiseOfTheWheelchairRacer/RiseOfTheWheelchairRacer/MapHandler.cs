﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;
using System.Threading;
using System.IO;

namespace RiseOfTheWheelchairRacer
{
    class MapHandler //Class for creating, and drawing the tile map
    {
        //Fields
        private Texture2D spikeTexture;
        private Texture2D wallTexture;
        private Texture2D platformTexture;
        private Texture2D elevatorTexture;
        private Texture2D spikeLeftTexture;
        private Texture2D spikeRightTexture;
        private Texture2D spawnTexture;

        private static int level;
        private static Rectangle spawn;
        static public Tile[,] tileArray;
        private Control controls;

        //Properties
        #region Properties
        public Texture2D SpikeTexture
        {
            get { return spikeTexture; }
            set { spikeTexture = value; }
        }
        public Texture2D WallTexture
        {
            get { return wallTexture; }
            set { wallTexture = value; }
        }
        public Texture2D PlatformTexture
        {
            get { return platformTexture; }
            set { platformTexture = value; }
        }
        public Texture2D ElevatorTexture
        {
            get { return elevatorTexture; }
            set { elevatorTexture = value; }
        }
        public Texture2D SpikeLeftTexture
        {
            get { return spikeLeftTexture; }
            set { spikeLeftTexture = value; }
        }
        public Texture2D SpikeRightTexture
        {
            get { return spikeRightTexture; }
            set { spikeRightTexture = value; }
        }
        public Texture2D SpawnTexture
        {
            get { return spawnTexture; }
            set { spawnTexture = value; }
        }
        public static int Level
        {
            get { return level; }
            set { level = value; }
        }
        public static Rectangle Spawn
        {
            get { return spawn; }
            set { spawn = value; }
        }
        public Control Controls
        {
            set { controls = value; }
        }
        #endregion

        //Constructor
        public MapHandler()
        {
            level = 1;
        }

        //Methods
        public void LoadTileTextures(Game1 game) //Loads all textures required by the MapHandler class
        {
            PlatformTexture = game.Content.Load<Texture2D>("Textures/Platform");
            SpikeTexture = game.Content.Load<Texture2D>("Textures/Spike");
            WallTexture = game.Content.Load<Texture2D>("Textures/Wall");
            ElevatorTexture = game.Content.Load<Texture2D>("Textures/Elevator");
            SpikeLeftTexture = game.Content.Load<Texture2D>("Textures/SpikeLeftTexture");
            SpikeRightTexture = game.Content.Load<Texture2D>("Textures/SpikeRightTexture");
            SpawnTexture = game.Content.Load<Texture2D>("Textures/Spawn");
        }

        public Tile[,] ReadMap(string mapFilePath) //Takes a map file and creates a map tile array based on it
        {
            //Reads map size from first line, and creates an array of appropriate size
            StreamReader input = new StreamReader(mapFilePath);

            string current = input.ReadLine();
            string[] coords = current.Split(',');
            int xSize = int.Parse(coords[0]);
            int ySize = int.Parse(coords[1]);

            Tile[,] tempArray = new Tile[xSize, ySize];
            int y = 0;

            //Populates tile array based on map file
            while (y < ySize)
            {
                current = input.ReadLine();
                for (int x = 0; x < xSize; x++)
                {
                    switch (current[x])
                    {
                        case 'W':
                            tempArray[x, y] = new Tile(x * GameVariables.tileSize, y * GameVariables.tileSize, wallTexture, 'W');
                            break;
                        case 'P':
                            tempArray[x, y] = new Tile(x * GameVariables.tileSize, y * GameVariables.tileSize, platformTexture, 'P');
                            break;
                        case 'S':
                            tempArray[x, y] = new Spike(x * GameVariables.tileSize, y * GameVariables.tileSize, SpikeDirections.up, spikeTexture);
                            break;
                        case '<':
                            tempArray[x, y] = new Spike(x * GameVariables.tileSize, y * GameVariables.tileSize, SpikeDirections.left, spikeRightTexture);
                            break;
                        case '>':
                            tempArray[x, y] = new Spike(x * GameVariables.tileSize, y * GameVariables.tileSize, SpikeDirections.right, spikeLeftTexture);
                            break;
                        case 'E':
                            tempArray[x, y] = new Tile(x * GameVariables.tileSize, y * GameVariables.tileSize, elevatorTexture, 'E');
                            break;
                        case 'X':
                            tempArray[x, y] = null;
                            spawn = new Rectangle(x * GameVariables.tileSize + 16, y * GameVariables.tileSize, GameVariables.tileSize / 2, GameVariables.tileSize);
                            break;
                        default: tempArray[x, y] = null; break;
                    }
                }
                y++;
            }
            input.Close();
            return tempArray;
        }

        public void DrawMap(Camera camera, SpriteBatch spriteBatch, Rectangle screen, SpriteFont font, Player player) //Draws all non null tiles in the array to the screen
        {
            //Draws each tile in the tile array that is intersecting with the screen (aka what the player can see)
            foreach (Tile tile in tileArray)
            {
                if (tile != null)
                {
                    if (tile.BoundingBox.Intersects(screen))
                    {
                        tile.Draw(spriteBatch);
                    }
                }
            }
            spriteBatch.Draw(SpawnTexture, Spawn, Color.White); //Spawn Txtures

            if (player.Difficulty == GameDifficulty.tutorial) // Tutorial text
            {
                if (RiseOfTheWheelchairRacer.MapHandler.Level == 1)
                {
                    if (controls.gamepad == true)
                    {
                        spriteBatch.DrawString(font, "Welcome to Rise of the Wheelchair Racer!\n\nEach level consists of jumping across platforms \nand avoiding spikes! Press A to jump.",
                            new Vector2(100, 100), Color.White);
                    }
                    else
                    {
                        spriteBatch.DrawString(font, "Welcome to Rise of the Wheelchair Racer!\n\nEach level consists of jumping across platforms \nand avoiding spikes! Press Space to jump.",
                            new Vector2(100, 100), Color.White);
                    }
                    spriteBatch.DrawString(font, "WATCH OUT\nFOR SPIKES!", new Vector2(430, 360), Color.White);
                    spriteBatch.DrawString(font, "Take the teleporter\nto reach the next level.", new Vector2(1470, 300), Color.White);
                }
                if (RiseOfTheWheelchairRacer.MapHandler.Level == 2)
                {
                    spriteBatch.DrawString(font, "Run off a platform, then jump! This will \nallow you to reach further distances!", new Vector2(70, 570), Color.White);
                    spriteBatch.DrawString(font, "You must land before you can jump again. \nIf you didn't, your jetpack would overheat!", new Vector2(380, 460), Color.White);
                }
                if (RiseOfTheWheelchairRacer.MapHandler.Level == 3)
                {
                    spriteBatch.DrawString(font, "On Easy Difficulty, Press 'S' to place a spawn point.\nYou will return there if you die! Warning: Placing\nspawn points adds 5 seconds to your time!", new Vector2(300, 260), Color.White);
                }
            }
        }

        public void SelectMap(int currentLevel, Player player)//Populates the tile array with the correct map
        {
            if (player.Difficulty == GameDifficulty.tutorial) //Tutorial Levels
            {
                switch (currentLevel)
                {
                    case 1: tileArray = ReadMap("Content/Maps/MapTut1.txt"); break;
                    case 2: tileArray = ReadMap("Content/Maps/MapTut2.txt"); level++; break;
                    case 3: tileArray = ReadMap("Content/Maps/MapTut3.txt"); level++; break;
                    default: player.GameWon = true; break;
                }
            }
            else
            {
                switch (currentLevel) // Regular levels
                {
                    case 1: tileArray = ReadMap("Content/Maps/Map1.txt"); break;
                    case 2: tileArray = ReadMap("Content/Maps/Map2.txt"); level++; break;
                    case 3: tileArray = ReadMap("Content/Maps/Map3.txt"); level++; break;
                    case 4: tileArray = ReadMap("Content/Maps/Map4.txt"); level++; break;
                    case 5: tileArray = ReadMap("Content/Maps/Map5.txt"); level++; break;
                    case 6: tileArray = ReadMap("Content/Maps/Map6.txt"); level++; break;
                    case 7: tileArray = ReadMap("Content/Maps/Map7.txt"); level++; break;
                    //case 8: tileArray = ReadMap("Content/Maps/Map8.txt"); level++; break;
                    default: player.GameWon = true; break;
                }
            }
            player.BoundingBox = spawn;
        }
    }
}
 
