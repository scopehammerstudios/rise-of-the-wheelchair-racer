﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace RiseOfTheWheelchairRacer
{
    class HighScore
    {
        //Fields
        SortedList listScores;
        SortedList listNames;
        TimeSpan[] scores;
        string[] names;

        //Properties
        public SortedList score { get { return listScores; } set { value = listScores; } }
        public SortedList name { get { return listNames; } set { value = listNames; } }

        //Methods
        public void ReadFile(string path)
        {
            listScores = new SortedList();
            listNames = new SortedList();
            scores = new TimeSpan[100];
            names = new string[100];
            try
            {
                StreamReader reader = new StreamReader(path);
                string line = "";
                int temp = 0;
                while ((line = reader.ReadLine()) != null)
                {
                    string[] lines = line.Split();
                    try
                    {
                        scores[temp] = TimeSpan.Parse(line);
                        listScores.Add(temp, scores[temp]);
                        temp++;
                    }
                    catch (FormatException)
                    {
                    }
                    catch (IndexOutOfRangeException)
                    { }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                        throw;
                    }
                }
                reader.Close();
            }
            catch (FileNotFoundException)
            {
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }

        }

        public void WriteFile(string path, string name, TimeSpan score)
        {
            try
            {
                StreamWriter writer = new StreamWriter(path);
                int ntemp = 0;
                int ntemp2 = 0;

                SortedList scoresTemp = new SortedList();

                for (int i = 0; i < listScores.Count; i++)
                {
                    if (score < scores[i])
                    {
                        ntemp++;
                        scoresTemp.Add(ntemp, scores[i]);
                        ntemp--;
                        ntemp2--;
                    }
                    else
                    {
                        scoresTemp.Add(i, scores[i]);
                    }
                    ntemp++;
                    ntemp2++;
                }
                scoresTemp.Remove(ntemp2);
                scoresTemp.Add(ntemp2, score);

                for (int i = 0; i < scoresTemp.Count; i++)
                {
                    writer.WriteLine(scoresTemp[i].ToString());
                }
                writer.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
        }
    }
}
