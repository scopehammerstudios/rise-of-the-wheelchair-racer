﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;

namespace RiseOfTheWheelchairRacer
{
    class Camera //Device used to control what the player sees
    {
        //Fields
        public Matrix transform;
        Vector2 center;
        int height;
        int width;
        bool isFullscreen;

        //Properties
        public Vector2 Center
        {
            get { return center; }
        }
        public bool IsFullscreen
        {
            get { return isFullscreen; }
        }

        //Constructor
        public Camera(int screenHeight, int screenWidth)
        {
            height = screenHeight;
            width = screenWidth;
        }

        public void Update(GameTime gameTime, Player character, ref Rectangle screen)
        {
            //Centers the camera on the player at all times
            center = new Vector2(character.BoundingBox.X + (character.BoundingBox.Width / 2) - width / 2, 
                character.BoundingBox.Y + (character.BoundingBox.Height / 2) - height / 2);

            int screenX = (int)(center.X);
            int screenY = (int)(center.Y);
            screen = new Rectangle(screenX, screenY, width, height);
            if (isFullscreen)
            {
                transform = Matrix.CreateScale(new Vector3(1, 1, 0)) * Matrix.CreateTranslation(new Vector3(-center.X, -center.Y, 0)) * Matrix.CreateScale(2.4f, 2.25f, 0);
            }
            else
            {
                transform = Matrix.CreateScale(new Vector3(1, 1, 0)) * Matrix.CreateTranslation(new Vector3(-center.X, -center.Y, 0)) * Matrix.CreateScale(1f, 1f, 0f);
            }
        }

        public void ToggleFullscreen(ref GraphicsDeviceManager graphics, Game1 game) //Changes the state of the screen resolution
        {
             isFullscreen = !isFullscreen;


            if (IsFullscreen)
            { 
                graphics.PreferredBackBufferHeight = 1080; 
                graphics.PreferredBackBufferWidth = 1920;
                //game.IsMouseVisible = false; //Mouse invisible in fullscreen
            }
            else 
            { 
                graphics.PreferredBackBufferHeight = 480; 
                graphics.PreferredBackBufferWidth = 800;
                //game.IsMouseVisible = true;
            }

            graphics.ToggleFullScreen();
            graphics.ApplyChanges();
        }
    }
}
