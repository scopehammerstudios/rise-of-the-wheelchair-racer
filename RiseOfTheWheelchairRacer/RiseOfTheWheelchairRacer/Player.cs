﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Media;

namespace RiseOfTheWheelchairRacer
{
    class Player : GamePiece //The main controlable gamepiece
    {
        //Fields
        #region Fields
        private CharacterState characterState;

        bool sprite;

        float time;
        float frameTime;
        int frameIndex;
        int frameHeight;
        int frameWidth;
        int sourceX;
        int sourceY;
        const int TOTAL_FRAMES_RUN = 7;
        const int TOTAL_FRAMES_IDLE = 4;
        const int Y_INDEX = 256;

        private double runVelocity = 200.0;
        private double runAcceleration = 10;
        private double fallVelocity = 0.0;
        private double fallAcceleration = -500;
        public int testVariable = 0;

        private Rectangle bottomCheck;
        private Rectangle leftCheck;
        private Rectangle rightCheck;
        private Rectangle topCheck;

        private SoundEffect jetpack;
        private SoundEffect teleport;
        private SoundEffect death;

        private bool goingLeft = false;
        private int currentLevel;
        private bool gameWon = false;

        private GameDifficulty difficulty;
        private int lives;
        #endregion

        //Properties
        #region Properties

        public double RunVelocity
        {
            get { return runVelocity; }
            set { runVelocity = value; }
        }
        public double RunAcceleration
        {
            get { return runAcceleration; }
            set { runAcceleration = value; }
        }
        public double FallVelocity
        {
            get { return fallVelocity; }
            set { fallVelocity = value; }
        }
        public double FallAcceleration
        {
            get { return fallAcceleration; }
            set { fallAcceleration = value; }
        }
        public CharacterState CharacterState
        {
            get { return characterState; }
            set { characterState = value; }
        }
        public Rectangle LandingCheck
        {
            get { return bottomCheck; }
            set { bottomCheck = value; }
        }
        public Rectangle LeftCheck
        {
            get { return leftCheck; }
            set { leftCheck = value; }
        }
        public Rectangle RightCheck
        {
            get { return rightCheck; }
            set { rightCheck = value; }
        }
        public Rectangle RoofCheck
        {
            get { return topCheck; }
            set { topCheck = value; }
        }
        public int CurrentLevel 
        {
            get { return currentLevel; } 
        }
        public bool GameWon 
        {
            get { return gameWon; }
            set { gameWon = value; }
        }
        public GameDifficulty Difficulty
        {
            get { return difficulty; }
            set { difficulty = value; }
        }
        public int Lives
        {
            get { return lives; }
            set { lives = value; }
        }
        #endregion
  
        //Constructor
        public Player(int xCoord, int yCoord)
            : base(xCoord, yCoord, 32, 64)
        {
            characterState = CharacterState.standing;
            RoofCheck = new Rectangle(xCoord, yCoord - 1, BoundingBox.Width - 20, 1);
            LandingCheck = new Rectangle(xCoord, yCoord + BoundingBox.Height + 1, BoundingBox.Width - 20, 1);
            RightCheck = new Rectangle(xCoord + BoundingBox.Width + 1, yCoord, 1, BoundingBox.Height - 20);
            LeftCheck = new Rectangle(xCoord - 1, yCoord, 1, BoundingBox.Height - 20);
            difficulty = GameDifficulty.easy;
            currentLevel = 1;
        }

        //Methods
        public void LoadPlayerContent(Game1 game)
        {
            sprite = false;
            switch (sprite)
            {
                case false:
                    Texture = game.Content.Load<Texture2D>("Textures/wheelChairRacer_Spritesheet_v5");
                    break;

                case true:
                    Texture = game.Content.Load<Texture2D>("Textures/wheelChairRacer_Spritesheet_wc_v1");
                    break;
            }
            
            jetpack = game.Content.Load<SoundEffect>("jetpack");
            teleport = game.Content.Load<SoundEffect>("teleport");
            death = game.Content.Load<SoundEffect>("death");
        }

        public void Draw(SpriteBatch spriteBatch, GameTime gameTime)
        {
            //Set up all nessecary vars
            frameWidth = 128;
            frameHeight = 256;
            frameTime = .2f;
            //player frame rectangle
            Rectangle source;
            //Switch statement handles drawing player based off current state

            #region Idle Cycler
            if (characterState == CharacterState.standing)
            {
                time += (float)gameTime.ElapsedGameTime.TotalSeconds;
                while (time > frameTime)
                {
                    // Play the next frame in the SpriteSheet
                    frameIndex++;

                    // reset elapsed time
                    time = 0f;
                }
                if (frameIndex > TOTAL_FRAMES_IDLE) frameIndex = 1;
                sourceX = frameIndex * frameWidth;
                sourceY = 0 * Y_INDEX;
            }
            #endregion

            #region Run Right Cycler
            else if (characterState == CharacterState.runningRight)
            {
                frameTime = (float)(.01 * (1 / (runVelocity / 2500)));
                time += (float)gameTime.ElapsedGameTime.TotalSeconds;
                while (time > frameTime)
                {
                    // Play the next frame in the SpriteSheet
                    frameIndex++;

                    // reset elapsed time
                    time = 0f;
                }
                if (frameIndex >= TOTAL_FRAMES_RUN) frameIndex = 1;
                sourceX = frameIndex * frameWidth;
                sourceY = 1 * Y_INDEX;
            }
            #endregion

            #region Run Left Cycler
            else if (characterState == CharacterState.runningLeft)
            {
                frameTime = (float)(.01 * (1 / (runVelocity / 2500)));
                time += (float)gameTime.ElapsedGameTime.TotalSeconds;
                while (time > frameTime)
                {
                    // Play the next frame in the SpriteSheet
                    frameIndex++;

                    // reset elapsed time
                    time = 0f;
                }
                if (frameIndex >= TOTAL_FRAMES_RUN) frameIndex = 1;
                sourceX = frameIndex * frameWidth;
                sourceY = 1 * Y_INDEX;
            }
            #endregion

            #region Jump Cycler
            else if (characterState == CharacterState.jumping)
            {
                sourceX = 128;
                sourceY = 2 * Y_INDEX + 2;
            }
            #endregion

            source = new Rectangle(sourceX, sourceY, frameWidth, frameHeight);

            // Process elapsed time
            Vector2 position = new Vector2(BoundingBox.X, BoundingBox.Y);
            Vector2 origin = new Vector2(0,0);

            switch (characterState)
            {
                case CharacterState.jumping:
                    if (goingLeft == true) { spriteBatch.Draw(Texture, BoundingBox, source, Color.White, 0.0f, origin, SpriteEffects.FlipHorizontally, 1.0f); }
                    else { spriteBatch.Draw(Texture, BoundingBox, source, Color.White, 0.0f, origin, SpriteEffects.None, 1.0f); }
                    break;
                case CharacterState.runningLeft:
                    spriteBatch.Draw(Texture, BoundingBox, source, Color.White, 0.0f, origin, SpriteEffects.FlipHorizontally, 1.0f);
                    break;
                case CharacterState.runningRight:
                    spriteBatch.Draw(Texture, BoundingBox, source, Color.White, 0.0f, origin, SpriteEffects.None, 1.0f);
                    break;
                case CharacterState.standing:
                    spriteBatch.Draw(Texture, BoundingBox, source, Color.White, 0.0f, origin, SpriteEffects.None, 1.0f);
                    break;
            }
        }

        //Handles the players input for running and jumping
        public void ProcessInput(GameTime time, ref MapHandler map, ref Timer timer, Control control)
        {
            //Ryan's gravity voodoo
            BoundingBoxY -= (int)(FallVelocity * time.ElapsedGameTime.TotalSeconds);
            testVariable = (int)(FallVelocity * time.ElapsedGameTime.TotalSeconds);
            //Austin: Added the second part of this equation. Gravity is acceleration times time squared, so I did just that. Also made it negative because well, positive made you fly
            FallVelocity += ((FallAcceleration * time.ElapsedGameTime.TotalSeconds) * -(FallAcceleration * time.ElapsedGameTime.TotalSeconds));
            if ((int)(FallVelocity * time.ElapsedGameTime.TotalSeconds) == 0)
                FallVelocity -= .7;
            UpdateCollisionBoxes();

            //Sets player spawn
            if (control.Spawn() == true && characterState == CharacterState.standing && (difficulty == GameDifficulty.easy || difficulty == GameDifficulty.tutorial))
            {
                if(!BoundingBox.Intersects(MapHandler.Spawn))
                {
                    RiseOfTheWheelchairRacer.MapHandler.Spawn = new Rectangle(BoundingBoxX, BoundingBoxY, GameVariables.tileSize / 2, GameVariables.tileSize);
                    timer.time += TimeSpan.FromSeconds(5);
                }
            }

            //Complex player input and condition checks to prevent hacky movement
            if (characterState != CharacterState.jumping && control.Jump())
            {
                fallVelocity = 1100;
                jetpack.Play();
                characterState = CharacterState.jumping;
            }

            else if (control.Left() && control.Right())
            {
                runVelocity = 0;
            }

            else if (control.Left())
            {
                if (control.gamepad)
                {
                    if (control.gpadStatePrev.ThumbSticks.Left.X > -.7f)
                        runVelocity = 200;
                }
                else
                {
                    if (control.kboardPrevious.IsKeyUp(Keys.A))
                        runVelocity = 200;
                }
                goingLeft = true;
                BoundingBoxX -= (int)(RunVelocity * time.ElapsedGameTime.TotalSeconds);     
                if (characterState != CharacterState.jumping)
                {
                    characterState = CharacterState.runningLeft;
                    runVelocity += runAcceleration;
                }
            }
            else if (control.Right())
            {
                if (control.gamepad)
                {
                    if (control.gpadStatePrev.ThumbSticks.Left.X < .7f)
                        runVelocity = 200;
                }
                else
                {
                    if (control.kboardPrevious.IsKeyUp(Keys.D))
                        runVelocity = 200;
                }
                goingLeft = false;
                BoundingBoxX += (int)(RunVelocity * time.ElapsedGameTime.TotalSeconds);
                if (characterState != CharacterState.jumping)
                {
                    characterState = CharacterState.runningRight;
                    runVelocity += runAcceleration;
                }
            }
            else if (characterState != CharacterState.jumping)
            {
                runVelocity = 200.0;
                characterState = CharacterState.standing;
            }
            UpdateCollisionBoxes();
        }

        public void UpdateCollisionBoxes() //Moves collision rectangles to proper locations around the player
        {
            topCheck.X = BoundingBoxX + 10;
            topCheck.Y = BoundingBoxY - 1;
            bottomCheck.X = BoundingBoxX + 10;
            bottomCheck.Y = BoundingBoxY + BoundingBox.Height;
            leftCheck.X = BoundingBoxX - 1;
            leftCheck.Y = BoundingBoxY + 10;
            rightCheck.X = BoundingBoxX + BoundingBox.Width;
            rightCheck.Y = BoundingBoxY + 10;
        }

        public void CollisionCheck(Rectangle rec) //Checks collisions using the collision boxes located around the player by comparing input rectangles to them
        {
            if (characterState == CharacterState.standing && fallVelocity < 0)
            {
                characterState = CharacterState.jumping;
            }
            if (RoofCheck.Intersects(rec))
            {
                fallVelocity = 0;
                BoundingBoxY += 1;
            }
            else if (LandingCheck.Intersects(rec) && fallVelocity < 500)
            {
                fallVelocity = 0;
                BoundingBoxY = rec.Y - rec.Height;
            }
            if (LandingCheck.Intersects(rec) && fallVelocity < 500 && !(RightCheck.Intersects(rec)) && !(LeftCheck.Intersects(rec)))
            {
                if (characterState != CharacterState.runningRight && characterState != CharacterState.runningLeft)
                {
                    characterState = CharacterState.standing;
                }
                fallVelocity = 0;
                BoundingBoxY = rec.Y - rec.Height;
            }
            if (RightCheck.Intersects(rec) && !LandingCheck.Intersects(rec))
            {
                BoundingBoxX -= 2;
                runVelocity = 0;
            }
            if (LeftCheck.Intersects(rec) && !LandingCheck.Intersects(rec))
            {
                BoundingBoxX += 2;
                runVelocity = 0;
            }
            if (!RightCheck.Intersects(rec) && !LeftCheck.Intersects(rec) && characterState == CharacterState.jumping && runVelocity < 100)
            {
                runVelocity = 175;
            }
        }

        public void SpecialCheck(ref MapHandler map, GameTime time, Menu menu) //Checks player collisions with tiles that require special handling (spikes, teleporters)
        {
            //If the player intersects a tile, they get sent back to the spawn
            foreach (Tile tile in MapHandler.tileArray)
            {
                if (tile != null && tile.TileID == 'S')
                {
                    if (LandingCheck.Intersects(tile.BoundingBox) || rightCheck.Intersects(tile.BoundingBox) || leftCheck.Intersects(tile.BoundingBox))
                    {
                        BoundingBox = MapHandler.Spawn;
                        runVelocity = 200;
                        death.Play();
                        lives--;
                        break;

                    }
                }
                if (tile != null && tile.TileID == 'E')
                {
                    if (BoundingBox.Intersects(tile.BoundingBox))
                    {
                        runVelocity = 0;
                        teleport.Play();
                        currentLevel++;
                        if (difficulty != GameDifficulty.hardPlus)
                        {
                            lives += 3;
                        }
                        if (currentLevel > 7)
                        {
                            gameWon = true;
                        }
                        else
                        {
                            map.SelectMap(currentLevel, this);
                            BoundingBox = MapHandler.Spawn;
                        }
                    }
                }
            }
        }

        public void CheckLevel(GameState gameState, ref MapHandler map)
        {
            if (gameState == GameState.menu)
            {
                currentLevel = 1;
                map.SelectMap(currentLevel, this);
                if (difficulty == GameDifficulty.hard)
                {
                    lives = 3;
                }
                if (difficulty == GameDifficulty.hardPlus)
                {
                    lives = 1;
                }
                BoundingBox = MapHandler.Spawn;
            }
        }
    }
}
