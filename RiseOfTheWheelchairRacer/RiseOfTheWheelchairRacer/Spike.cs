﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;

namespace RiseOfTheWheelchairRacer
{
    enum SpikeDirections //Determines the way the spikes face
    {
        up,
        down,
        left,
        right
    };

    class Spike : Tile //Deadly tiles
    {
        //Fields
        private SpikeDirections direction;

        //Properties
        public SpikeDirections Direction
        {
            get { return direction; }
        }

        //Constructor
        public Spike(int xCoord, int yCoord, SpikeDirections direction, Texture2D texture)
            : base(xCoord, yCoord, texture, 'S')
        {
            this.direction = direction;

            switch (this.direction) //Appropriately orients the spike
            {
                case SpikeDirections.up: BoundingBox = new Rectangle(xCoord, yCoord + 32, GameVariables.tileSize, 32); break;
                case SpikeDirections.left: BoundingBox = new Rectangle(xCoord + 32, yCoord, 32, GameVariables.tileSize); break;
                default: BoundingBox = new Rectangle(xCoord, yCoord, 32, GameVariables.tileSize); break;
            }
        }

        //Methods
        public override void Draw(SpriteBatch spriteBatch)
        {
            base.Draw(spriteBatch);
        }
    }
}
