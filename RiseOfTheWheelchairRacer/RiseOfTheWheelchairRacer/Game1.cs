﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;
using System.Threading;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Media;

namespace RiseOfTheWheelchairRacer
{
    public class Game1 : Game
    {
        //Fields
        #region Fields
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        Control control;

        PlayerIndex playerIndex;
        Timer timer;

        Menu menu = new Menu();
        public int screenWidth;
        public int screenHeight;
        public Rectangle screen;
        GameState gameState;

        SoundEffect backgroundSound;
        Camera camera;
        MapHandler map;
        Player player;
        SpriteFont font;
        SoundEffectInstance backSound;
        bool musicMute;
        #endregion

        //Constructor
        public Game1()
            : base()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        //Methods
        protected override void Initialize()
        {
            playerIndex = PlayerIndex.One;
            gameState = GameState.startScreen;
            screenWidth = 800;
            screenHeight = 480;
            graphics.SynchronizeWithVerticalRetrace = true; //Vsync. Removes screen tearing
            screen = new Rectangle((int)GameVariables.screenPosition.X, (int)GameVariables.screenPosition.Y, screenWidth + 200, screenHeight + 10);
            camera = new Camera(screenHeight, screenWidth);
            control = new Control(playerIndex);
            musicMute = false;
            this.IsMouseVisible = true;
            base.Initialize();
        }

        protected override void LoadContent()
        {
            //Creates all necessary class instances
            spriteBatch = new SpriteBatch(GraphicsDevice);
            map = new MapHandler();
            font = this.Content.Load<SpriteFont>("SpriteFont1");
            timer = new Timer(font);

            //Loads textures
            menu.LoadMenuTextures(this);
            map.LoadTileTextures(this);
            map.Controls = control;

            //Plays background music
            backgroundSound = this.Content.Load<SoundEffect>("backgroundSound"); 
            backSound = backgroundSound.CreateInstance();
            backSound.IsLooped = true;
            backSound.Play();

            //Creates the player and loads all required content (sounds and textures)
            player = new Player(0,0);
            player.LoadPlayerContent(this);
        }

        protected override void UnloadContent()
        {
        }

        protected override void Update(GameTime gameTime)
        {
            if (control.Mute()) //Music Muting
            {
                if (musicMute == true) 
                    { backSound.Play(); musicMute = false; }
                else 
                    { backSound.Stop(); musicMute = true; }
            }
            if (control.Fullscreen())//Handling of screen size changes
            {
                camera.ToggleFullscreen(ref graphics, this);
            }
            camera.Update(gameTime, player, ref screen);
            switch (gameState)
            {
                default: break;
                case GameState.game:
                    ProcessInput(gameTime); //Gets player input
                    foreach (Tile tile in MapHandler.tileArray) //Colisions for all tiles being drawn on the screen that are not teleporters
                    {
                        if (tile != null && tile.TileID != 'E' && tile.BoundingBox.Intersects(screen))
                        {
                            player.CollisionCheck(tile.BoundingBox);
                        }
                    }
                    player.SpecialCheck(ref map, gameTime, menu); //Checks for teleporter and spike collisions
                    if ((player.Difficulty == GameDifficulty.hard && player.Lives < 1)|| (player.Difficulty == GameDifficulty.hardPlus && player.Lives < 1))
                    {
                        gameState = GameState.gameOver;
                    }
                    break;
                case GameState.menu:
                    player.CheckLevel(gameState, ref map);
                    break;
            }
            if (menu.gg) { Exit(); }
            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Black);
            //Begins the spritebatch, and implements the camera
            spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend, null, null, null, null, camera.transform);

            //switch based on game state
            switch (gameState)
            {
                //All menus draw in menu.DrawMenu
                default: menu.MenuDraw(spriteBatch, font, ref gameState, screen, ref map, ref timer, player, control); break;
                case GameState.game:
                    menu.selectIndex = 0;
                    map.DrawMap(camera, spriteBatch, screen, font, player);
                    player.Draw(spriteBatch, gameTime);
                    timer.KeepTime(gameTime, spriteBatch, camera, ref player);
                    Vector2 levelLength = font.MeasureString("Level: " + MapHandler.Level);
                    spriteBatch.DrawString(font, "Level: " + MapHandler.Level, new Vector2(camera.Center.X + screenWidth - 10 - levelLength.X, camera.Center.Y + 10), Color.White);
                    
                    if (player.GameWon == true) { gameState = GameState.gameOver; }
                    if (player.Difficulty == GameDifficulty.hard || player.Difficulty == GameDifficulty.hardPlus)
                    {
                        Vector2 livesLength = font.MeasureString("Lives: " + player.Lives);
                        spriteBatch.DrawString(font, "Lives: " + player.Lives, new Vector2((camera.Center.X + (screenWidth / 2) - (livesLength.X / 2)), camera.Center.Y + 10), Color.White);
                        Console.WriteLine(screenWidth);
                    }
                    break;
            }

            spriteBatch.End();
            base.Draw(gameTime);
        }

        public void ProcessInput(GameTime gameTime) //Gets called for in the update method
        {
            control.UpdateInputState();
            player.ProcessInput(gameTime, ref map, ref timer, control); //Player movement
            if (control.Pause()) //Pause
            { gameState = GameState.pause; }
        }
    }
}