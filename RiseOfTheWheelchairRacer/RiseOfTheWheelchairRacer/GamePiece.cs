﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;


namespace RiseOfTheWheelchairRacer
{
    abstract class GamePiece //Everything onscreen (Things with textures)
    {
        //Fields
        private Rectangle boundingBox;
        private Texture2D texture;

        //Properties
        #region Properties
        public Rectangle BoundingBox
        {
            get { return boundingBox; }
            set { boundingBox = value; }
        }

        public Texture2D Texture
        {
            get { return texture; }
            set { texture = value; }
        }

        public int BoundingBoxX // x component for boundingBox
        {
            get { return boundingBox.X; }
            set { boundingBox.X = value; }
        }

        public int BoundingBoxY // y component for boundingBox
        {
            get { return boundingBox.Y; }
            set { boundingBox.Y = value; }
        }
        #endregion

        //Constructor
        public GamePiece(int xCoord, int yCoord, int xSize, int ySize)
        {
            boundingBox = new Rectangle(xCoord, yCoord, xSize, ySize);
        }

        //Methods
        public virtual void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(Texture, BoundingBox, Color.White);
        }
    }
}
