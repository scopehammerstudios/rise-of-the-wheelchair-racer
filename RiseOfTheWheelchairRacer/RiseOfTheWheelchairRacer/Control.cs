﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;
using System.Threading;

namespace RiseOfTheWheelchairRacer
{
    class Control //Handles all player input. Each method corresponds to an action on both keyboard and gamepad
    {
        #region Fields
        PlayerIndex playerIndex;
        
        public KeyboardState kboardPrevious;
        public KeyboardState kboardCurrent;

        public GamePadState gpadStatePrev;
        public GamePadState gpadStateCurrent;

        public string Text { get; set; }
        public int MustPass = 125;
        DateTime prevUpdate = DateTime.Now;

        public bool gamepad;
        #endregion

        public Control(PlayerIndex _playerIndex)
        {
            playerIndex = _playerIndex;
        }

        public void UpdateInputState()
        {
            kboardPrevious = kboardCurrent;
            kboardCurrent = Keyboard.GetState();

            gpadStatePrev = gpadStateCurrent;
            gpadStateCurrent = GamePad.GetState(playerIndex);
            DetectGamePad();
        }

        public void DetectGamePad()
        {
            if (gpadStateCurrent.IsConnected == true)
            {
                gamepad = true;
            }
            else gamepad = false;
        }

        public bool Spawn()
        {
            if (gamepad)
            {
                if (gpadStateCurrent.IsButtonDown(Buttons.X))
                {
                    return true;
                }

                else return false;
            }

            else
            {
                if (kboardCurrent.IsKeyDown(Keys.S) && kboardPrevious.IsKeyUp(Keys.S))
                {
                    return true;
                }

                else return false;
            }
        }

        public bool Left()
        {
            if (gamepad)
            {
                if (gpadStateCurrent.ThumbSticks.Left.X < -0.7f)
                {
                    return true;
                }

                else return false;
            }

            else
            {
                if (kboardCurrent.IsKeyDown(Keys.A))
                {
                    return true;
                }

                else return false;
            }
        }

        public bool Right()
        {
            if (gamepad)
            {
                if (gpadStateCurrent.ThumbSticks.Left.X > 0.7f)
                {
                    return true;
                }

                else return false;
            }

            else
            {
                if (kboardCurrent.IsKeyDown(Keys.D))
                {
                    return true;
                }

                else return false;
            }

        }

        public bool MenuRight()
        {
            if (gamepad)
            {
                if (gpadStateCurrent.ThumbSticks.Left.X > 0.7f && gpadStatePrev.ThumbSticks.Left.X < 0.7f)
                {
                    return true;
                }

                else return false;
            }

            else
            {
                if (kboardCurrent.IsKeyDown(Keys.D) && kboardPrevious.IsKeyUp(Keys.D))
                {
                    return true;
                }

                else return false;
            }
        }

        public bool MenuLeft()
        {
            if (gamepad)
            {
                if (gpadStateCurrent.ThumbSticks.Left.X < -0.7f && gpadStatePrev.ThumbSticks.Left.X > -0.7f)
                {
                    return true;
                }

                else return false;
            }

            else
            {
                if (kboardCurrent.IsKeyDown(Keys.A) && kboardPrevious.IsKeyUp(Keys.A))
                {
                    return true;
                }

                else return false;
            }
        }

        public bool MenuUp()
        {
            if (gamepad)
            {
                if (gpadStateCurrent.ThumbSticks.Left.Y > 0.9f && gpadStatePrev.ThumbSticks.Left.Y < 0.9f)
                {
                    return true;
                }
            }
            else
            {
                if (kboardCurrent.IsKeyDown(Keys.W) && kboardPrevious.IsKeyUp(Keys.W))
                {
                    return true;
                }
            }
            return false;
        }

        public bool MenuDown()
        {
            if (gamepad)
            {
                if (gpadStateCurrent.ThumbSticks.Left.Y < -0.9f && gpadStatePrev.ThumbSticks.Left.Y > -0.9f)
                {
                    return true;
                }
            }
            else
            {
                if (kboardCurrent.IsKeyDown(Keys.S) && kboardPrevious.IsKeyUp(Keys.S))
                {
                    return true;
                }
            }
            return false;
        }

        public bool Enter()
        {
            if (gamepad)
            {
                if (gpadStateCurrent.IsButtonDown(Buttons.A) && gpadStatePrev.IsButtonUp(Buttons.A))
                {
                    return true;
                }

                else return false;
            }

            else
            {
                if (kboardCurrent.IsKeyDown(Keys.Space) && kboardPrevious.IsKeyUp(Keys.Space))
                {
                    return true;
                }

                else return false;
            }
        }

        public bool Jump()
        {
            if (gamepad)
            {
                if (gpadStateCurrent.IsButtonDown(Buttons.A) && gpadStatePrev.IsButtonUp(Buttons.A))
                {
                    return true;
                }

                else return false;
            }

            else
            {
                if (kboardCurrent.IsKeyDown(Keys.Space) && kboardPrevious.IsKeyUp(Keys.Space))
                {
                    return true;
                }

                else return false;
            }
        }

        public bool Pause()
        {
            if (gamepad)
            {
                if (gpadStateCurrent.IsButtonDown(Buttons.Start) && gpadStatePrev.IsButtonUp(Buttons.Start))
                {
                    return true;
                }

                else return false;
            }

            else
            {
                if (kboardCurrent.IsKeyDown(Keys.Escape) && kboardPrevious.IsKeyUp(Keys.Escape))
                {
                    return true;
                }

                else return false;
            }
        }

        public bool Mute()
        {
            if (gamepad)
            {
                if (gpadStateCurrent.IsButtonDown(Buttons.Y) && gpadStatePrev.IsButtonUp(Buttons.Y))
                {
                    return true;
                }

                else return false;
            }

            else
            {
                if (kboardCurrent.IsKeyDown(Keys.M) && kboardPrevious.IsKeyUp(Keys.M))
                {
                    return true;
                }

                else return false;
            }
        }

        public bool Fullscreen()
        {
            if (gamepad)
            {
                if (gpadStateCurrent.IsButtonDown(Buttons.Back) && gpadStatePrev.IsButtonUp(Buttons.Back))
                {
                    return true;
                }

                else return false;
            }

            else
            {
                if (kboardCurrent.IsKeyDown(Keys.F) && kboardPrevious.IsKeyUp(Keys.F))
                {
                    return true;
                }

                else return false;
            }
        }

        public string Convert(Keys[] keys)
        {
            string output = "";
            bool usesShift = (keys.Contains(Keys.LeftShift) || keys.Contains(Keys.RightShift));

            foreach (Keys key in keys)
            {
                if (kboardPrevious.IsKeyUp(key))
                    continue;
                if (key >= Keys.A && key <= Keys.Z)
                    output += key.ToString();
                else if (key >= Keys.NumPad0 && key <= Keys.NumPad9)
                    output += ((int)(key - Keys.NumPad0)).ToString();
                else if (key >= Keys.D0 && key <= Keys.D9)
                {
                    string num = ((int)(key - Keys.D0)).ToString();
                    #region special num chars
                    if (usesShift)
                    {
                        switch (num)
                        {
                            case "1": num = "!"; break;
                            case "2": num = "@"; break;
                            case "3": num = "#"; break;
                            case "4": num = "$"; break;
                            case "5": num = "%"; break;
                            case "6": num = "^"; break;
                            case "7": num = "&"; break;
                            case "8": num = "*"; break;
                            case "9": num = "("; break;
                            case "0": num = ")"; break;
                            default: break;
                        }
                    }
                    #endregion
                    output += num;
                }

                else if (key == Keys.OemPeriod)
                    output += ".";
                else if (key == Keys.OemTilde)
                    output += "'";
                else if (key == Keys.Space)
                    output += " ";
                else if (key == Keys.OemMinus)
                    output += "-";
                else if (key == Keys.OemPlus)
                    output += "+";
                else if (key == Keys.OemQuestion && usesShift)
                    output += "?";
                else if (key == Keys.Back) //backspace
                    output += "\b";

                if (!usesShift) //shouldn't need to upper because it's automagically in upper case
                    output = output.ToLower();

            }

            return output;

        }

        public void Update()
        {
            kboardCurrent = Keyboard.GetState();
            string input = Convert(kboardCurrent.GetPressedKeys());
            string prevInput = Convert(kboardPrevious.GetPressedKeys());
            DateTime now = DateTime.Now;
            int time = MustPass;
            if (input == "\b")
                time -= 25;
            if (/*now.Subtract(prevUpdate).Milliseconds >= time*/true)
            {
                foreach (char x in input)
                {
                    if (x == '\b')
                    {
                        if (Text.Length >= 1)
                        {
                            Text = Text.Remove(Text.Length - 1, 1);
                        }
                    }
                    else
                        Text += x;
                }
                if (!string.IsNullOrEmpty(input))
                    prevUpdate = now;
            }
            kboardPrevious = kboardCurrent;
        }
        

    }
}
