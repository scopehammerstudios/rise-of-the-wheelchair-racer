﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;

namespace RiseOfTheWheelchairRacer
{
    class Tile : EnvironmentPiece //Tile for tile system
    {
        //Fields
        private char tileID;
        private Rectangle landingBox;

        //Properties
        public char TileID
        {
            get { return tileID; }
        }
        public Rectangle LandingBox 
        {
            get { return landingBox; }
            set { landingBox = value; }
        }

        //Constructor
        public Tile(int xCoord, int yCoord, Texture2D texture, char identifier)
            : base(xCoord, yCoord)
        {
            this.Texture = texture;
            tileID = identifier;
            landingBox = new Rectangle(xCoord, yCoord + 1, BoundingBox.Width, 1);
        }

        //Methods
        public override void Draw(SpriteBatch spriteBatch)
        {
            base.Draw(spriteBatch);
        }
    }
}
