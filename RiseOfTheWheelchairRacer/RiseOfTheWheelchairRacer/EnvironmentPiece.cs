﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;

namespace RiseOfTheWheelchairRacer
{
    abstract class EnvironmentPiece : GamePiece //All gamepieces that are not the player
    {
        //Constructor
        public EnvironmentPiece(int xCoord, int yCoord)
            : base(xCoord, yCoord, GameVariables.tileSize, GameVariables.tileSize) { }

        //Methods
        public override void Draw(SpriteBatch spriteBatch)
        {
            base.Draw(spriteBatch);
        }
    }
}
