﻿namespace SHS_MapEditor
{
    partial class SHSMapEditorRWR
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SHSMapEditorRWR));
            this.inputBox = new System.Windows.Forms.TextBox();
            this.writeButton = new System.Windows.Forms.Button();
            this.importButton = new System.Windows.Forms.Button();
            this.fileNameLabel = new System.Windows.Forms.Label();
            this.fileNameBox = new System.Windows.Forms.TextBox();
            this.xBox = new System.Windows.Forms.TextBox();
            this.xLabel = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.yBox = new System.Windows.Forms.TextBox();
            this.wallPicture = new System.Windows.Forms.PictureBox();
            this.wallLabel = new System.Windows.Forms.Label();
            this.platformLabel = new System.Windows.Forms.Label();
            this.platformBox = new System.Windows.Forms.PictureBox();
            this.spikeBox = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.elevatorLabel = new System.Windows.Forms.Label();
            this.elevatorBox = new System.Windows.Forms.PictureBox();
            this.emptyLabel = new System.Windows.Forms.Label();
            this.clearButton = new System.Windows.Forms.Button();
            this.spawnLabel = new System.Windows.Forms.Label();
            this.lSpikeLabel = new System.Windows.Forms.Label();
            this.leftSpike = new System.Windows.Forms.PictureBox();
            this.rSpikeLabel = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.spawnBox = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.wallPicture)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.platformBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.elevatorBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.leftSpike)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spawnBox)).BeginInit();
            this.SuspendLayout();
            // 
            // inputBox
            // 
            this.inputBox.Font = new System.Drawing.Font("Consolas", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.inputBox.Location = new System.Drawing.Point(15, 41);
            this.inputBox.Multiline = true;
            this.inputBox.Name = "inputBox";
            this.inputBox.Size = new System.Drawing.Size(1001, 581);
            this.inputBox.TabIndex = 0;
            // 
            // writeButton
            // 
            this.writeButton.Location = new System.Drawing.Point(966, 14);
            this.writeButton.Name = "writeButton";
            this.writeButton.Size = new System.Drawing.Size(50, 20);
            this.writeButton.TabIndex = 1;
            this.writeButton.Text = "Write";
            this.writeButton.UseVisualStyleBackColor = true;
            this.writeButton.Click += new System.EventHandler(this.writeButton_Click);
            // 
            // importButton
            // 
            this.importButton.Location = new System.Drawing.Point(911, 14);
            this.importButton.Name = "importButton";
            this.importButton.Size = new System.Drawing.Size(50, 20);
            this.importButton.TabIndex = 2;
            this.importButton.Text = "Import";
            this.importButton.UseVisualStyleBackColor = true;
            this.importButton.Click += new System.EventHandler(this.importButton_Click);
            // 
            // fileNameLabel
            // 
            this.fileNameLabel.AutoSize = true;
            this.fileNameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fileNameLabel.Location = new System.Drawing.Point(12, 18);
            this.fileNameLabel.Name = "fileNameLabel";
            this.fileNameLabel.Size = new System.Drawing.Size(58, 13);
            this.fileNameLabel.TabIndex = 3;
            this.fileNameLabel.Text = "File name: ";
            // 
            // fileNameBox
            // 
            this.fileNameBox.Location = new System.Drawing.Point(69, 15);
            this.fileNameBox.Name = "fileNameBox";
            this.fileNameBox.Size = new System.Drawing.Size(621, 20);
            this.fileNameBox.TabIndex = 4;
            // 
            // xBox
            // 
            this.xBox.Location = new System.Drawing.Point(716, 14);
            this.xBox.Name = "xBox";
            this.xBox.Size = new System.Drawing.Size(80, 20);
            this.xBox.TabIndex = 6;
            // 
            // xLabel
            // 
            this.xLabel.AutoSize = true;
            this.xLabel.Location = new System.Drawing.Point(696, 18);
            this.xLabel.Name = "xLabel";
            this.xLabel.Size = new System.Drawing.Size(17, 13);
            this.xLabel.TabIndex = 7;
            this.xLabel.Text = "X:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(802, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(17, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "Y:";
            // 
            // yBox
            // 
            this.yBox.Location = new System.Drawing.Point(825, 14);
            this.yBox.Name = "yBox";
            this.yBox.Size = new System.Drawing.Size(80, 20);
            this.yBox.TabIndex = 9;
            // 
            // wallPicture
            // 
            this.wallPicture.Image = ((System.Drawing.Image)(resources.GetObject("wallPicture.Image")));
            this.wallPicture.ImageLocation = "";
            this.wallPicture.Location = new System.Drawing.Point(1026, 49);
            this.wallPicture.Name = "wallPicture";
            this.wallPicture.Size = new System.Drawing.Size(64, 64);
            this.wallPicture.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.wallPicture.TabIndex = 10;
            this.wallPicture.TabStop = false;
            // 
            // wallLabel
            // 
            this.wallLabel.AutoSize = true;
            this.wallLabel.Location = new System.Drawing.Point(1023, 33);
            this.wallLabel.Name = "wallLabel";
            this.wallLabel.Size = new System.Drawing.Size(45, 13);
            this.wallLabel.TabIndex = 11;
            this.wallLabel.Text = "Wall: W";
            // 
            // platformLabel
            // 
            this.platformLabel.AutoSize = true;
            this.platformLabel.Location = new System.Drawing.Point(1023, 117);
            this.platformLabel.Name = "platformLabel";
            this.platformLabel.Size = new System.Drawing.Size(58, 13);
            this.platformLabel.TabIndex = 12;
            this.platformLabel.Text = "Platform: P";
            // 
            // platformBox
            // 
            this.platformBox.Image = ((System.Drawing.Image)(resources.GetObject("platformBox.Image")));
            this.platformBox.Location = new System.Drawing.Point(1023, 132);
            this.platformBox.Name = "platformBox";
            this.platformBox.Size = new System.Drawing.Size(64, 64);
            this.platformBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.platformBox.TabIndex = 13;
            this.platformBox.TabStop = false;
            // 
            // spikeBox
            // 
            this.spikeBox.AutoSize = true;
            this.spikeBox.Location = new System.Drawing.Point(1023, 200);
            this.spikeBox.Name = "spikeBox";
            this.spikeBox.Size = new System.Drawing.Size(47, 13);
            this.spikeBox.TabIndex = 14;
            this.spikeBox.Text = "Spike: S";
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(1022, 215);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(64, 64);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 15;
            this.pictureBox1.TabStop = false;
            // 
            // elevatorLabel
            // 
            this.elevatorLabel.AutoSize = true;
            this.elevatorLabel.Location = new System.Drawing.Point(1020, 528);
            this.elevatorLabel.Name = "elevatorLabel";
            this.elevatorLabel.Size = new System.Drawing.Size(59, 13);
            this.elevatorLabel.TabIndex = 16;
            this.elevatorLabel.Text = "Elevator: E";
            // 
            // elevatorBox
            // 
            this.elevatorBox.Image = ((System.Drawing.Image)(resources.GetObject("elevatorBox.Image")));
            this.elevatorBox.Location = new System.Drawing.Point(1022, 542);
            this.elevatorBox.Name = "elevatorBox";
            this.elevatorBox.Size = new System.Drawing.Size(64, 64);
            this.elevatorBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.elevatorBox.TabIndex = 17;
            this.elevatorBox.TabStop = false;
            // 
            // emptyLabel
            // 
            this.emptyLabel.AutoSize = true;
            this.emptyLabel.Location = new System.Drawing.Point(1019, 609);
            this.emptyLabel.Name = "emptyLabel";
            this.emptyLabel.Size = new System.Drawing.Size(48, 13);
            this.emptyLabel.TabIndex = 18;
            this.emptyLabel.Text = "Empty: - ";
            // 
            // clearButton
            // 
            this.clearButton.Location = new System.Drawing.Point(1022, 14);
            this.clearButton.Name = "clearButton";
            this.clearButton.Size = new System.Drawing.Size(50, 20);
            this.clearButton.TabIndex = 19;
            this.clearButton.Text = "Clear";
            this.clearButton.UseVisualStyleBackColor = true;
            this.clearButton.Click += new System.EventHandler(this.clearButton_Click);
            // 
            // spawnLabel
            // 
            this.spawnLabel.AutoSize = true;
            this.spawnLabel.Location = new System.Drawing.Point(1022, 447);
            this.spawnLabel.Name = "spawnLabel";
            this.spawnLabel.Size = new System.Drawing.Size(53, 13);
            this.spawnLabel.TabIndex = 20;
            this.spawnLabel.Text = "Spawn: X";
            // 
            // lSpikeLabel
            // 
            this.lSpikeLabel.AutoSize = true;
            this.lSpikeLabel.Location = new System.Drawing.Point(1022, 282);
            this.lSpikeLabel.Name = "lSpikeLabel";
            this.lSpikeLabel.Size = new System.Drawing.Size(67, 13);
            this.lSpikeLabel.TabIndex = 21;
            this.lSpikeLabel.Text = "Left Spike: >";
            // 
            // leftSpike
            // 
            this.leftSpike.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.leftSpike.Image = ((System.Drawing.Image)(resources.GetObject("leftSpike.Image")));
            this.leftSpike.Location = new System.Drawing.Point(1023, 298);
            this.leftSpike.Name = "leftSpike";
            this.leftSpike.Size = new System.Drawing.Size(64, 64);
            this.leftSpike.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.leftSpike.TabIndex = 22;
            this.leftSpike.TabStop = false;
            // 
            // rSpikeLabel
            // 
            this.rSpikeLabel.AutoSize = true;
            this.rSpikeLabel.Location = new System.Drawing.Point(1022, 365);
            this.rSpikeLabel.Name = "rSpikeLabel";
            this.rSpikeLabel.Size = new System.Drawing.Size(74, 13);
            this.rSpikeLabel.TabIndex = 23;
            this.rSpikeLabel.Text = "Right Spike: <";
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(1024, 380);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(64, 64);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 24;
            this.pictureBox2.TabStop = false;
            // 
            // spawnBox
            // 
            this.spawnBox.Image = ((System.Drawing.Image)(resources.GetObject("spawnBox.Image")));
            this.spawnBox.Location = new System.Drawing.Point(1022, 464);
            this.spawnBox.Name = "spawnBox";
            this.spawnBox.Size = new System.Drawing.Size(64, 64);
            this.spawnBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.spawnBox.TabIndex = 25;
            this.spawnBox.TabStop = false;
            // 
            // SHSMapEditorRWR
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1094, 631);
            this.Controls.Add(this.spawnBox);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.rSpikeLabel);
            this.Controls.Add(this.leftSpike);
            this.Controls.Add(this.lSpikeLabel);
            this.Controls.Add(this.spawnLabel);
            this.Controls.Add(this.clearButton);
            this.Controls.Add(this.emptyLabel);
            this.Controls.Add(this.elevatorBox);
            this.Controls.Add(this.elevatorLabel);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.spikeBox);
            this.Controls.Add(this.platformBox);
            this.Controls.Add(this.platformLabel);
            this.Controls.Add(this.wallLabel);
            this.Controls.Add(this.wallPicture);
            this.Controls.Add(this.yBox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.xLabel);
            this.Controls.Add(this.xBox);
            this.Controls.Add(this.fileNameBox);
            this.Controls.Add(this.fileNameLabel);
            this.Controls.Add(this.importButton);
            this.Controls.Add(this.writeButton);
            this.Controls.Add(this.inputBox);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "SHSMapEditorRWR";
            this.Text = "Rise of the Wheelchair Racer Map Editor";
            this.Load += new System.EventHandler(this.SHSMapEditorRWR_Load);
            ((System.ComponentModel.ISupportInitialize)(this.wallPicture)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.platformBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.elevatorBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.leftSpike)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spawnBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox inputBox;
        private System.Windows.Forms.Button writeButton;
        private System.Windows.Forms.Button importButton;
        private System.Windows.Forms.Label fileNameLabel;
        private System.Windows.Forms.TextBox fileNameBox;
        private System.Windows.Forms.TextBox xBox;
        private System.Windows.Forms.Label xLabel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox yBox;
        private System.Windows.Forms.PictureBox wallPicture;
        private System.Windows.Forms.Label wallLabel;
        private System.Windows.Forms.Label platformLabel;
        private System.Windows.Forms.PictureBox platformBox;
        private System.Windows.Forms.Label spikeBox;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label elevatorLabel;
        private System.Windows.Forms.PictureBox elevatorBox;
        private System.Windows.Forms.Label emptyLabel;
        private System.Windows.Forms.Button clearButton;
        private System.Windows.Forms.Label spawnLabel;
        private System.Windows.Forms.Label lSpikeLabel;
        private System.Windows.Forms.PictureBox leftSpike;
        private System.Windows.Forms.Label rSpikeLabel;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox spawnBox;
    }
}

