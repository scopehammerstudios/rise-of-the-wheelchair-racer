﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace SHS_MapEditor
{
    public partial class SHSMapEditorRWR : Form
    {
        public string filename;
        //toolsActive Variable determines whether the button is active or not
        public bool toolsActive;
        //form obj
        //coordinate list field
        public int width;
        public int height;
        public string[] coordsArrayString;
        public int[] coordsArrayInt;

        public SHSMapEditorRWR()
        {
            InitializeComponent();
        }

        private void writeButton_Click(object sender, EventArgs e)
        {
            try
            {
                //creates file name
                filename = fileNameBox.Text;
                //Writes textbox to file
                StreamWriter write = new StreamWriter(@".\"+ filename +".txt");
                //writes size to textfile
                write.WriteLine("{0},{1}", xBox.Text, yBox.Text);
                //writes other shit to textfile
                write.Write(inputBox.Text);
                write.Close();
                MessageBox.Show("File written!");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void importButton_Click(object sender, EventArgs e)
        {
            try
            {
                //creates file name
                filename = fileNameBox.Text;
                //reads a text file
                StreamReader read = new StreamReader(@".\" + filename +".txt");

                #region Coordinate Reader
                //this section of code is respopnsible for reading the first line of the text file
                //that contains the size of the map file. 
                string coords = read.ReadLine();
                coordsArrayString = coords.Split(',');
                coordsArrayInt = new int[2];
                for (int x = 0; x < coordsArrayString.Length; x++)
                {
                    coordsArrayInt[x] = int.Parse(coordsArrayString[x]);
                }
                width = coordsArrayInt[0];
                height = coordsArrayInt[1];
                xBox.Text = coordsArrayString[0];
                yBox.Text = coordsArrayString[1];
                #endregion

                inputBox.Text = read.ReadToEnd();
                read.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void SHSMapEditorRWR_Load(object sender, EventArgs e)
        {

        }

        private void clearButton_Click(object sender, EventArgs e)
        {
            fileNameBox.Clear();
            xBox.Clear();
            yBox.Clear();
            inputBox.Clear();
        }
    }
}
