﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;

namespace RiseOfTheWheelchairRacer
{
    class Menu
    {
        //Fields
        #region Fields
        public Texture2D pixel;
        Texture2D menu;
        Texture2D objective;
        Texture2D controls;
        Texture2D highScore;
        Texture2D pause;
        Texture2D gameOver;
        Texture2D controlGameOver;
        Texture2D selectBar;
        Texture2D wasd;
        Texture2D gamepad;
        Texture2D difficulty;
        public int selectIndex = 0;
        public bool gg = false;
        public bool scoreCheck = false;

        //MainMenu
        private Rectangle mainPlayRect = new Rectangle(70, 200, 300, 30);
        private Rectangle mainControlsRect = new Rectangle(70, 263, 300, 30);
        private Rectangle mainHighscoreRect = new Rectangle(70, 328, 300, 30);
        private Rectangle mainExitRect = new Rectangle(70, 390, 300, 30);

        //Pause Menu
        private Rectangle pauseResumeRect = new Rectangle(70, 180, 300, 30);
        private Rectangle pauseMainRect = new Rectangle(70, 246, 300, 30);
        private Rectangle pauseExitRect = new Rectangle(70, 320, 300, 30);

        //Difficulty Menu
        private Rectangle difficultyTut = new Rectangle(250, 184, 300, 40);
        private Rectangle difficultyEasy = new Rectangle(250, 234, 300, 40);
        private Rectangle difficultyMedium = new Rectangle(250, 280, 300, 40);
        private Rectangle difficultyHard = new Rectangle(250, 328, 300, 40);
        private Rectangle difficultyHardPlus = new Rectangle(250, 378, 300, 40);

        //Game Over Menu
        private Vector2 scorePos = new Vector2(148, 285);

        //Start Screen
        private Rectangle startScreen = new Rectangle(100, 50, 600, 380);
        #endregion

        //Methods
        public void LoadMenuTextures(Game1 game) //Loads all textures required by the menu system
        {
            pixel = new Texture2D(game.GraphicsDevice, 1, 1, false, SurfaceFormat.Color);
            pixel.SetData(new[] { Color.White });
            menu = game.Content.Load<Texture2D>("Textures/Main menu");
            objective = game.Content.Load<Texture2D>("Textures/Objective");
            controls = game.Content.Load<Texture2D>("Textures/Control");
            highScore = game.Content.Load<Texture2D>("Textures/Hiscore");
            pause = game.Content.Load<Texture2D>("Textures/Pause");
            gameOver = game.Content.Load<Texture2D>("Textures/Game Over");
            controlGameOver = game.Content.Load<Texture2D>("Textures/Control Game Over");
            selectBar = game.Content.Load<Texture2D>("Textures/SelectBar");
            wasd = game.Content.Load<Texture2D>("Textures/wasd-retro-v2");
            gamepad = game.Content.Load<Texture2D>("Textures/controller_new-Recovered-alphabg-v5");
            difficulty = game.Content.Load<Texture2D>("Textures/Difficulty");
        }
        public void MenuDraw(SpriteBatch spriteBatch, SpriteFont font, ref GameState currentState, Rectangle screen, ref MapHandler map, ref Timer timer, Player player, Control control)
        {
            //Draws the appropriate menu based on game state
            switch (currentState)
            {
                case GameState.startScreen: //Splash screen at the beginning that shows the control schemes
                    if (ProcessMenuInput(control))
                    {
                        currentState = GameState.menu;
                        selectIndex = 0;
                    }
                    Rectangle tempStart = new Rectangle(screen.X + startScreen.X, screen.Y + startScreen.Y, startScreen.Width, startScreen.Height);
                    if (control.gamepad == true)
                    {
                        spriteBatch.Draw(gamepad, tempStart, Color.White);
                    }
                    else
                    {
                        spriteBatch.Draw(wasd, tempStart, Color.White);
                    }
                    break;
                case GameState.menu: //Main menu
                    scoreCheck = false;
                    spriteBatch.Draw(menu, screen, Color.White);;
                    if (ProcessMenuInput(control))
                    {
                        switch (selectIndex % 4) //Handles navigation based off selectIndex
                        {
                            case 0: currentState = GameState.difficulty;
                                selectIndex = 0;
                                break;

                            case -3:
                            case 1: currentState = GameState.objective; selectIndex = 0; break;

                            case -2:
                            case 2: currentState = GameState.highScore; selectIndex = 0; break;

                            default: gg = true; break;
                        }
                    }
                    //Moves the selection bar based on the current selection index
                    switch (selectIndex % 4)
                    {
                        case 0:
                            int xPlay = screen.X + 178;
                            int yPlay = screen.Y + 8;
                            Rectangle tempPlay = new Rectangle(xPlay + mainPlayRect.X, yPlay + mainPlayRect.Y, mainPlayRect.Width, mainPlayRect.Height);
                            spriteBatch.Draw(selectBar, tempPlay, Color.White); break;

                        case -3:
                        case 1:
                            int xCon = screen.X + 178;
                            int yCon = screen.Y + 8;
                            Rectangle tempControls = new Rectangle(xCon + mainControlsRect.X, yCon + mainControlsRect.Y, mainControlsRect.Width, mainControlsRect.Height);
                            spriteBatch.Draw(selectBar, tempControls, Color.White); break;

                        case -2:
                        case 2:
                            int xObj = screen.X + 178;
                            int yObj = screen.Y + 8;
                            Rectangle tempObjective = new Rectangle(xObj + mainHighscoreRect.X, yObj + mainHighscoreRect.Y, mainHighscoreRect.Width, mainHighscoreRect.Height);
                            spriteBatch.Draw(selectBar, tempObjective, Color.White); break;

                        case -1:
                        case 3:
                            int xExit = screen.X + 178;
                            int yExit = screen.Y + 8;
                            Rectangle tempExit = new Rectangle(xExit + mainExitRect.X, yExit + mainExitRect.Y, mainExitRect.Width, mainExitRect.Height);
                            spriteBatch.Draw(selectBar, tempExit, Color.White); break;

                    }
                    break;

                case GameState.difficulty: //Difficulty menu
                    spriteBatch.Draw(difficulty, screen, Color.White);
                    MapHandler.Level = 1;
                    if (ProcessMenuInput(control))
                    {
                        switch (selectIndex % 5) // Handles the selection of a difficulty based on the selectIndex
                        {
                            case 0:
                                player.Difficulty = GameDifficulty.tutorial;
                                break;

                            case 1:
                            case -4:
                                player.Difficulty = GameDifficulty.easy;
                                break;

                            case 2:
                            case -3:
                                player.Difficulty = GameDifficulty.medium;
                                break;

                            case 3:
                            case -2:
                                player.Difficulty = GameDifficulty.hard;
                                player.Lives = 3; ;
                                break;

                            case 4:
                            case -1:
                                player.Difficulty = GameDifficulty.hardPlus;
                                player.Lives = 1;
                                break;
                        }
                        map.SelectMap(player.CurrentLevel, player);
                        timer.time = TimeSpan.Zero;
                        currentState = GameState.game;
                    }

                    switch (selectIndex % 5) // Handles drawing the select bar on the difficulty menu
                    {
                        case 0:
                            Rectangle tempTut = new Rectangle(screen.X + difficultyTut.X, screen.Y + difficultyTut.Y, difficultyTut.Width, difficultyTut.Height);
                            spriteBatch.Draw(selectBar, tempTut, Color.White);
                            break;

                        case 1:
                        case -4:
                            Rectangle tempEasy = new Rectangle(screen.X + difficultyEasy.X, screen.Y + difficultyEasy.Y, difficultyEasy.Width, difficultyEasy.Height);
                            spriteBatch.Draw(selectBar, tempEasy, Color.White);
                            break;

                        case 2:
                        case -3:
                            Rectangle tempMedium = new Rectangle(screen.X + difficultyMedium.X, screen.Y + difficultyMedium.Y, difficultyMedium.Width, difficultyMedium.Height);
                            spriteBatch.Draw(selectBar, tempMedium, Color.White);
                            break;

                        case 3:
                        case -2:
                            Rectangle tempHard = new Rectangle(screen.X + difficultyHard.X, screen.Y + difficultyHard.Y, difficultyHard.Width, difficultyHard.Height);
                            spriteBatch.Draw(selectBar, tempHard, Color.White);
                            break;

                        case 4:
                        case -1:
                            Rectangle tempHardPlus = new Rectangle(screen.X + difficultyHardPlus.X, screen.Y + difficultyHardPlus.Y, difficultyHardPlus.Width, difficultyHardPlus.Height);
                            spriteBatch.Draw(selectBar, tempHardPlus, Color.White);
                            break;
                    }
                    break;


                case GameState.gameOver: //Game over screen            
                    if (ProcessMenuInput(control))
                    {
                        player.GameWon = false;

                        if (timer.Score == TimeSpan.Zero)
                        {
                            selectIndex = 0;
                            currentState = GameState.menu;
                        }
                        else
                        {
                            switch (player.Difficulty)
                            {
                                case GameDifficulty.easy: selectIndex = 0; break;
                                case GameDifficulty.medium: selectIndex = 1; break;
                                case GameDifficulty.hard: selectIndex = 2; break;
                                case GameDifficulty.hardPlus: selectIndex = 3; break;
                            }
                            if (player.Difficulty != GameDifficulty.tutorial)
                            {
                                currentState = GameState.highScore;
                            }
                            else
                            {
                                currentState = GameState.menu;
                            }
                        }

                    }
                    else if (control.Pause())
                    {
                        gg = true;
                    }
                    if (control.gamepad == true)
                    {
                        spriteBatch.Draw(controlGameOver, screen, Color.White);
                    }
                    else
                    {
                        spriteBatch.Draw(gameOver, screen, Color.White);
                    }
                    if (timer.Score != TimeSpan.Zero && player.Difficulty != GameDifficulty.tutorial)
                    {
                        //Centers text
                        Vector2 stringSize = font.MeasureString("Your Time Was " + timer.Score.ToString(@"hh\:mm\:ss\.FF").Trim('0', ':', '0'));
                        Vector2 temp = new Vector2(screen.X + (screen.Width / 2) - (stringSize.X / 2), screen.Y + ((screen.Height / 8) * 5));
                        spriteBatch.DrawString(font, "Your Time Was " + timer.Score.ToString(@"hh\:mm\:ss\.FF").Trim('0', ':', '0'), temp, Color.White);
                        HighScore hiscore2 = new HighScore();
                        if (scoreCheck == false)
                        {
                            switch (player.Difficulty)
                            {
                                case GameDifficulty.easy: hiscore2.ReadFile("easy.dat"); hiscore2.WriteFile("easy.dat", "Alex", timer.Score); break;
                                case GameDifficulty.medium: hiscore2.ReadFile("medium.dat"); hiscore2.WriteFile("medium.dat", "Alex", timer.Score); break;
                                case GameDifficulty.hard: hiscore2.ReadFile("hard.dat"); hiscore2.WriteFile("hard.dat", "Alex", timer.Score); break;
                                case GameDifficulty.hardPlus: hiscore2.ReadFile("HARD+.dat"); hiscore2.WriteFile("HARD+.dat", "Alex", timer.Score); break;

                            }
                        }
                        scoreCheck = true;
                    }
                    else if (player.Difficulty == GameDifficulty.tutorial && timer.Score != TimeSpan.Zero)
                    {
                        Vector2 stringSize = font.MeasureString("You Beat The Tutorial!");
                        Vector2 temp = new Vector2(screen.X + (screen.Width / 2) - (stringSize.X / 2), screen.Y + ((screen.Height / 8) * 5));
                        spriteBatch.DrawString(font, "You Beat The Tutorial!", temp, Color.White);
                    }
                    else
                    {
                        //centers text
                        Vector2 stringSize = font.MeasureString("You Didn't Finish!");
                        Vector2 temp = new Vector2(screen.X + (screen.Width / 2) - (stringSize.X / 2), screen.Y + ((screen.Height / 8) * 5));
                        spriteBatch.DrawString(font, "You Didn't Finish!", temp, Color.White);
                    }
                    break;

                case GameState.objective:
                    if (ProcessMenuInput(control) || control.MenuRight())
                    {
                        currentState = GameState.controls;
                    }
                    else if (control.Pause())
                    {
                        currentState = GameState.menu;
                    }
                    spriteBatch.Draw(objective, screen, Color.White);
                    break;

                case GameState.controls:
                    if (ProcessMenuInput(control) || control.MenuRight())
                    {
                        currentState = GameState.difficulty;
                    }
                    else if (control.MenuLeft())
                    {
                        currentState = GameState.objective;
                    }
                    spriteBatch.Draw(controls, screen, Color.White);
                    Rectangle tempControl = new Rectangle(screen.X + startScreen.X, screen.Y + startScreen.Y + 20, startScreen.Width, startScreen.Height - 40);
                    spriteBatch.Draw(pixel, tempControl, Color.Black);
                    if (control.gamepad == true)
                    {
                        spriteBatch.Draw(gamepad, tempControl, Color.White);
                    }
                    else
                    {
                        spriteBatch.Draw(wasd, tempControl, Color.White);
                    }
                    break;

                case GameState.pause: //Pause menu screen
                    spriteBatch.Draw(pause, screen, Color.White);
                    if (ProcessMenuInput(control))
                    {
                        switch (selectIndex % 3) //Handles menu navigation on the pause menu
                        {
                            case 0: currentState = GameState.game; selectIndex = 0; break;
                            case 1:
                            case -2:currentState = GameState.menu; selectIndex = 0; break;
                            default: gg = true; break;
                        }
                    }
                    //Draws selection bar on pause screen based on player input
                    switch (selectIndex % 3) // Handles drawing select bar based on selectIndex on the pause screen
                    {
                        case 0:
                            int xRes = screen.X + 178;
                            int yRes = screen.Y + 8;
                            Rectangle tempRes = new Rectangle(xRes + pauseResumeRect.X, yRes + pauseResumeRect.Y, pauseResumeRect.Width, pauseResumeRect.Height);
                            spriteBatch.Draw(selectBar, tempRes, Color.White); break;

                        case -2:
                        case 1:
                            int xMain = screen.X + 178;
                            int yMain = screen.Y + 8;
                            Rectangle tempMain = new Rectangle(xMain + pauseMainRect.X, yMain + pauseMainRect.Y, pauseMainRect.Width, pauseMainRect.Height);
                            spriteBatch.Draw(selectBar, tempMain, Color.White); break;

                        case -1:
                        case 2:
                            int xPause = screen.X + 178;
                            int yPause = screen.Y + 8;
                            Rectangle tempPause = new Rectangle(xPause + pauseExitRect.X, yPause + pauseExitRect.Y, pauseExitRect.Width, pauseExitRect.Height);
                            spriteBatch.Draw(selectBar, tempPause, Color.White); break;
                    }
                    break;
                case GameState.highScore:
                    spriteBatch.Draw(highScore, screen, Color.White);
                    HighScore hiscore = new HighScore();
                    switch (selectIndex % 4)
                    {
                        #region easy hiscore
                        case 0:
                            hiscore.ReadFile("easy.dat");
                            Vector2 S = font.MeasureString("EASY");
                            Vector2 V = new Vector2(screen.X + (screen.Width / 2) - (S.X / 2), screen.Y + ((screen.Height / 2)));
                            spriteBatch.DrawString(font, "EASY", V, Color.White);
                            for (int i = 0; i < 5; i++)
                            {
                                TimeSpan[] top5 = new TimeSpan[5];

                                if (hiscore.score[i] != null)
                                {
                                    top5[i] = TimeSpan.Parse(hiscore.score[i].ToString());

                                    Vector2 s = font.MeasureString(i + 1 + ") " + top5[i].ToString(@"hh\:mm\:ss\.FF").Trim('0', ':', '0'));
                                    Vector2 temp = new Vector2(screen.X + ((screen.Width / 2) - (s.X / 2)), screen.Y + (screen.Height / 12) * (i + 7));

                                    spriteBatch.DrawString(font, i + 1 + ") " + top5[i].ToString(@"hh\:mm\:ss\.FF").Trim('0', ':', '0'), temp, Color.White);
                                    if (hiscore.score[i].ToString() == timer.Score.ToString() && player.Difficulty == GameDifficulty.easy)
                                    {
                                        spriteBatch.DrawString(font, i + 1 + ") " + top5[i].ToString(@"hh\:mm\:ss\.FF").Trim('0', ':', '0'), temp, Color.Red);

                                    }
                                }
                            }
                            if (ProcessMenuInput(control))
                            {
                                currentState = GameState.menu;
                                selectIndex = 0;
                            }
                            break;
                        #endregion
                        #region medium hiscore
                        case -3:
                        case 1:
                            hiscore.ReadFile("medium.dat");
                            S = font.MeasureString("MEDIUM");
                            V = new Vector2(screen.X + (screen.Width / 2) - (S.X / 2), screen.Y + ((screen.Height / 2)));
                            spriteBatch.DrawString(font, "MEDIUM", V, Color.White);
                            for (int i = 0; i < 5; i++)
                            {
                                TimeSpan[] top5 = new TimeSpan[5];

                                if (hiscore.score[i] != null)
                                {
                                    top5[i] = TimeSpan.Parse(hiscore.score[i].ToString());

                                    Vector2 s = font.MeasureString(i + 1 + ") " + top5[i].ToString(@"hh\:mm\:ss\.FF").Trim('0', ':', '0'));
                                    Vector2 temp = new Vector2(screen.X + ((screen.Width / 2) - (s.X / 2)), screen.Y + (screen.Height / 12) * (i + 7));

                                    spriteBatch.DrawString(font, i + 1 + ") " + top5[i].ToString(@"hh\:mm\:ss\.FF").Trim('0', ':', '0'), temp, Color.White);
                                    if (hiscore.score[i].ToString() == timer.Score.ToString() && player.Difficulty == GameDifficulty.medium)
                                    {
                                        spriteBatch.DrawString(font, i + 1 + ") " + top5[i].ToString(@"hh\:mm\:ss\.FF").Trim('0', ':', '0'), temp, Color.Red);

                                    }
                                }
                            }
                            if (ProcessMenuInput(control))
                            {
                                currentState = GameState.menu;
                                selectIndex = 0;
                            }
                            break;
                        #endregion
                        #region hard hiscore
                        case -2:
                        case 2:
                            hiscore.ReadFile("hard.dat");
                            S = font.MeasureString("HARD");
                            V = new Vector2(screen.X + (screen.Width / 2) - (S.X / 2), screen.Y + ((screen.Height / 2)));
                            spriteBatch.DrawString(font, "HARD", V, Color.White);
                            for (int i = 0; i < 5; i++)
                            {
                                TimeSpan[] top5 = new TimeSpan[5];

                                if (hiscore.score[i] != null)
                                {
                                    top5[i] = TimeSpan.Parse(hiscore.score[i].ToString());

                                    Vector2 s = font.MeasureString(i + 1 + ") " + top5[i].ToString(@"hh\:mm\:ss\.FF").Trim('0', ':', '0'));
                                    Vector2 temp = new Vector2(screen.X + ((screen.Width / 2) - (s.X / 2)), screen.Y + (screen.Height / 12) * (i + 7));

                                    spriteBatch.DrawString(font, i + 1 + ") " + top5[i].ToString(@"hh\:mm\:ss\.FF").Trim('0', ':', '0'), temp, Color.White);
                                    if (hiscore.score[i].ToString() == timer.Score.ToString() && player.Difficulty == GameDifficulty.hard)
                                    {
                                        spriteBatch.DrawString(font, i + 1 + ") " + top5[i].ToString(@"hh\:mm\:ss\.FF").Trim('0', ':', '0'), temp, Color.Red);

                                    }
                                }
                            }
                            if (ProcessMenuInput(control))
                            {
                                currentState = GameState.menu;
                                selectIndex = 0;
                            }
                            break;
                        #endregion
                        #region Hard Plus hiscore
                        case -1:
                        case 3:
                            hiscore.ReadFile("HARD+.dat");
                            S = font.MeasureString("HARD+");
                            V = new Vector2(screen.X + (screen.Width / 2) - (S.X / 2), screen.Y + ((screen.Height / 2)));
                            spriteBatch.DrawString(font, "HARD+", V, Color.White);
                            for (int i = 0; i < 5; i++)
                            {
                                TimeSpan[] top5 = new TimeSpan[5];

                                if (hiscore.score[i] != null)
                                {
                                    top5[i] = TimeSpan.Parse(hiscore.score[i].ToString());

                                    Vector2 s = font.MeasureString(i + 1 + ") " + top5[i].ToString(@"hh\:mm\:ss\.FF").Trim('0', ':', '0'));
                                    Vector2 temp = new Vector2(screen.X + ((screen.Width / 2) - (s.X / 2)), screen.Y + (screen.Height / 12) * (i + 7));

                                    spriteBatch.DrawString(font, i + 1 + ") " + top5[i].ToString(@"hh\:mm\:ss\.FF").Trim('0', ':', '0'), temp, Color.White);
                                    if (hiscore.score[i].ToString() == timer.Score.ToString() && player.Difficulty == GameDifficulty.hardPlus)
                                    {
                                        spriteBatch.DrawString(font, i + 1 + ") " + top5[i].ToString(@"hh\:mm\:ss\.FF").Trim('0', ':', '0'), temp, Color.Red);

                                    }
                                }
                            }
                            if (ProcessMenuInput(control))
                            {
                                currentState = GameState.menu;
                                selectIndex = 0;
                            }
                            break;
                        #endregion
                    }
                    break;
            }
        }

        //Increments or decrements the select index based on player input. Only returns true when space is pressed to enter something
        public bool ProcessMenuInput(Control control)
        {
            bool exit = false;
            control.UpdateInputState();

            if (control.MenuUp())
            {
                selectIndex--;
            }
            else if (control.MenuDown())
            {
                selectIndex++;
            }
            else if (control.Enter())
            {
                exit = true;
            }

            return exit;
        }
    }
}